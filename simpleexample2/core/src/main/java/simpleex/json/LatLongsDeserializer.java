package simpleex.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;

public class LatLongsDeserializer extends JsonDeserializer<LatLongs> {

  private LatLongDeserializer latLongDeserializer = new LatLongDeserializer();

  @Override
  public LatLongs deserialize(JsonParser jsonParser,
      DeserializationContext deserContext) throws IOException, JsonProcessingException {
    JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
    if (jsonNode instanceof ArrayNode) {
      ArrayNode latLongsArray = (ArrayNode) jsonNode;
      Collection<LatLong> latLongs = new ArrayList<>(latLongsArray.size());
      for (JsonNode latLongNode : latLongsArray) {
        LatLong latLong = latLongDeserializer.deserialize(latLongNode);
        latLongs.add(latLong);
      }
      return new LatLongs(latLongs);
    }
    return null;
  }
}
