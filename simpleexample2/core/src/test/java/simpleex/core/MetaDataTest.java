package simpleex.core;

import java.util.Arrays;
import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MetaDataTest {

  private MetaData metaData;

  @BeforeEach
  public void setUp() {
    metaData = new MetaData();
  }

  @Test
  public void testHasTags() {
    assertTrue(metaData.hasTags());
    assertFalse(metaData.hasTags("aTag"));

    metaData.addTags("aTag", "bTag");
    assertTrue(metaData.hasTags());
    assertTrue(metaData.hasTags("aTag"));
    assertFalse(metaData.hasTags("aTag", "cTag"));

    metaData.removeTags("aTag");
    assertTrue(metaData.hasTags());
    assertFalse(metaData.hasTags("aTag"));
    assertTrue(metaData.hasTags("bTag"));
  }

  // relies on a certain order
  private void check(final Iterator<String> it, final String... ss) {
    final Iterator<String> it1 = Arrays.asList(ss).iterator();
    while (it1.hasNext() && it.hasNext()) {
      assertEquals(it1.next(), it.next());
    }
    assertEquals(it1.hasNext(), it.hasNext());
  }

  // relies on a certain order
  private void checkTags(final String... tags) {
    check(metaData.tags(), tags);
  }

  @Test
  public void testMetaData() {
    assertTrue(metaData.isEmpty());
    checkTags();
  }

  @Test
  public void testSetTags() {
    metaData.setTags("aTag", "bTag");
    checkTags("aTag", "bTag");
    metaData.setTags("cTag", "bTag");
    checkTags("cTag", "bTag");
    metaData.setTags();
    checkTags();
  }

  @Test
  public void testAddRemoveTags() {
    metaData.addTags("aTag", "bTag");
    checkTags("aTag", "bTag");
    metaData.addTags("cTag");
    checkTags("aTag", "bTag", "cTag");
    metaData.removeTags("bTag");
    checkTags("aTag", "cTag");
    metaData.removeTags("cTag");
    checkTags("aTag");
    metaData.removeTags("aTag");
    assertTrue(metaData.isEmpty());
  }

  @Test
  public void testHasProperty() {
    assertFalse(metaData.hasProperty("aProp"));
    metaData.setProperty("aProp", "aValue");
    assertTrue(metaData.hasProperty("aProp"));
  }

  @Test
  public void testGetSetProperty() {
    metaData.setProperty("aProp", "aValue");
    assertEquals("aValue", metaData.getProperty("aProp"));
    metaData.setProperty("bProp", "bValue");
    assertEquals("bValue", metaData.getProperty("bProp"));
    metaData.setProperty("aProp", "anotherValue");
    assertEquals("anotherValue", metaData.getProperty("aProp"));

    assertEquals(-1, metaData.getIntegerProperty("iProp", -1));
    metaData.setProperty("iProp", "notAnInteger");
    assertEquals(-1, metaData.getIntegerProperty("dProp", -1));
    metaData.setIntegerProperty("iProp", 42);
    assertEquals(42, metaData.getIntegerProperty("iProp", -1));

    assertEquals(-1.0, metaData.getDoubleProperty("dProp", -1.0), 0.0);
    metaData.setProperty("dProp", "notADouble");
    assertEquals(-1.0, metaData.getDoubleProperty("dProp", -1.0), 0.0);
    metaData.setDoubleProperty("dProp", 42.0);
    assertEquals(42.0, metaData.getDoubleProperty("dProp", -1.0), 0.0);

    assertEquals(false, metaData.getBooleanProperty("bProp", false));
    metaData.setProperty("bProp", "notABoolean");
    assertEquals(false, metaData.getBooleanProperty("bProp", true));
    metaData.setBooleanProperty("bProp", true);
    assertEquals(true, metaData.getBooleanProperty("bProp", false));
  }

  @Test
  public void testSetRemoveProperty() {
    metaData.setProperty("aProp", "aValue");
    assertTrue(metaData.hasProperty("aProp"));
    metaData.removeProperty("aProp");
    assertFalse(metaData.hasProperty("aProp"));
    assertTrue(metaData.isEmpty());
  }

  // relies on a certain order
  private void checkPropertyNames(final String... propertyNames) {
    check(metaData.propertyNames(), propertyNames);
  }

  @Test
  public void testPropertyNames() {
    checkPropertyNames();
    metaData.setProperty("aProp", "aValue");
    checkPropertyNames("aProp");
    metaData.removeProperty("aProp");
    checkPropertyNames();
  }
}
