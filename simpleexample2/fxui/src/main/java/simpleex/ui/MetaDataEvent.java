package simpleex.ui;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 * Event class intended to handle metadata related actions.
 */
public class MetaDataEvent extends Event {

  private static final long serialVersionUID = 1L;

  public MetaDataEvent(Object source, EventTarget target, EventType<? extends Event> eventType) {
    super(source, target, eventType);
  }

  public MetaDataEvent(EventType<? extends Event> eventType) {
    super(eventType);
  }
}
