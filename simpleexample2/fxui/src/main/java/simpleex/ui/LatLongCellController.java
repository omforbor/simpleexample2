package simpleex.ui;


import java.util.Iterator;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import simpleex.core.LatLong;
import simpleex.core.MetaData;

/**
 * The controller for the renderer in the ListView cells containing the locations.
 *
 * @author Adrian Stoica
 *
 */
public class LatLongCellController {

  /**
   * The main container of the cell UI.
   */
  private Region root;

  /**
   * The horizontal box containing the coordinates and the add/edit button.
   */
  private HBox horizontalBox;

  /**
   * The button that will allow opening the editor for the selected item.
   */
  private Button editMetadataButton;

  /**
   * Container for the additional metadata.
   */
  private VBox verticalBox;

  /**
   * The label for the name property.
   */
  private Label nameLabel;

  /**
   * The label for the coordinates.
   */
  private Label coordinatesLabel;

  /**
   * The label for the description.
   */
  private Label descriptionLabel;

  /**
   * The current LatLong object that needs to be displayed.
   */
  private LatLong latLong;
  private LatLongCell latLongCell;

  /**
   * Create a new controller for managing a LatLong list cell.
   *
   * @param latLongCell the cell to be controlled
   */
  public LatLongCellController(LatLongCell latLongCell) {
    super();
    this.latLongCell = latLongCell;
  }

  /**
   * Set the location object.
   *
   * @param latLong the reference to the object to be displayed
   */
  public void setLatLong(LatLong latLong) {
    this.latLong = latLong;
  }

  /**
   * Prepare the cell UI.
   *
   * @param selected flag indicating that the item is selected
   */
  protected void prepareView(boolean selected) {
    this.horizontalBox = new HBox();
    this.horizontalBox.setSpacing(3.0);
    this.coordinatesLabel = new Label(this.latLong.toString());
    coordinatesLabel.setPrefWidth(180.0);
    this.horizontalBox.getChildren().add(coordinatesLabel);

    if (this.latLong.hasMetaData()) {
      if (this.latLong.getMetaData().hasCustomProperties()) {
        this.horizontalBox.getChildren()
            .add(new ImageView(new Image(
              getClass().getResourceAsStream((selected ? "i_selected.png" : "i.png"))
            )));
      }
    }

    if (selected) {
      this.editMetadataButton = new Button("...");
      this.horizontalBox.getChildren().add(editMetadataButton);
      editMetadataButton.setOnAction(event -> {
        try {
          FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MetaDataEditor.fxml"));
          Stage stage = new Stage();
          stage.initModality(Modality.APPLICATION_MODAL);
          stage.initStyle(StageStyle.DECORATED);
          stage.setTitle("Location MetaData Editor");
          Parent root1 = fxmlLoader.load();
          MetaDataEditorController metaDataEditorController = fxmlLoader.getController();
          metaDataEditorController.setLatLong(this.latLong);
          Scene scene = new Scene(root1);
          stage.setScene(scene);
          scene.getStylesheets().add(
              getClass().getResource("/simpleex/ui/tags/tagsbar.css").toExternalForm()
          );
          stage.show();
          stage.addEventHandler(
              MetaDataEditorController.METADATA_SAVED, new EventHandler<MetaDataEvent>() {
                @Override
                public void handle(MetaDataEvent event) {
                  if (event.getEventType() == MetaDataEditorController.METADATA_SAVED) {
                    System.out.println("metadata saved click");
                    LatLongCellController.this.latLongCell.updateItem(latLong, false);
                  }
                }
              }
          );
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      });
    }

    if (this.latLong.hasMetaData()) {
      this.verticalBox = new VBox();
      MetaData metaData = this.latLong.getMetaData();
      if (metaData.hasProperty(MetaData.NAME_PROPERTY)) {
        nameLabel = new Label();
        nameLabel.setText(metaData.getProperty(MetaData.NAME_PROPERTY));
        verticalBox.getChildren().add(nameLabel);
      }
      verticalBox.getChildren().add(this.horizontalBox);
      if (metaData.hasProperty(MetaData.DESCRIPTION_PROPERTY)) {
        descriptionLabel = new Label();
        descriptionLabel.setText(metaData.getProperty(MetaData.DESCRIPTION_PROPERTY));
        descriptionLabel.setWrapText(true);
        descriptionLabel.setMaxHeight(50.0);
        descriptionLabel.setManaged(true);
        descriptionLabel.setMaxWidth(200.0);
        verticalBox.getChildren().add(descriptionLabel);
      }
      final Iterator<String> tags = metaData.tags();
      if (tags.hasNext()) {
        HBox tagsBox = new HBox();
        tagsBox.setSpacing(5.0);
        while (tags.hasNext()) {
          Label tagLabel = new Label(tags.next());
          tagLabel.setStyle("-fx-background-color: #43464b; \n" + " -fx-text-fill: white;\n"
              + " -fx-border-radius: 3 3 3 3; \n" + "    -fx-background-radius: 3 3 3 03; ");
          tagLabel.setPadding(new Insets(0.0, 3.0, 0.0, 3.0));
          tagsBox.getChildren().add(tagLabel);
        }
        verticalBox.getChildren().add(tagsBox);
      }
      this.root = this.verticalBox;
    } else {
      this.root = this.horizontalBox;
    }
  }

  /**
   * Get the UI for the cell based on selection and the available info in the latLong object.
   *
   * @param selected flag indicating that the item is selected
   * @return the root container for the cell UI
   */
  public Region getCellView(boolean selected) {
    prepareView(selected);
    return this.root;
  }
}
